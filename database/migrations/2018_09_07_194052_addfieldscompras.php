<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addfieldscompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras', function($table){
            $table->string('category');
            $table->string('code');
            $table->string('accesories')->nullable();
            $table->string('serienumber')->nullable();
            $table->string('repuestos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function($table){
            $table->dropColumn('category');
            $table->dropColumn('code');
            $table->dropColumn('accesories');
            $table->dropColumn('serienumber');
            $table->dropColumn('repuestos');
        });
    }
}
