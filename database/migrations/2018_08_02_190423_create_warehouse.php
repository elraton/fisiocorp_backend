<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('category');
            $table->string('name');
            $table->text('description');
            $table->text('accesories')->nullable();
            $table->integer('quantity');
            $table->integer('quantity_now');
            $table->string('status');
            $table->string('serie_number')->nullable();
            $table->text('spares')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('warehouses');
    }
}
