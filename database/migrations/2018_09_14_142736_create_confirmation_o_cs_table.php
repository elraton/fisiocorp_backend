<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmationOCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmation_o_cs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client');
            $table->decimal('total', 8, 2);
            $table->string('code');
            $table->string('ship');
            $table->string('bill');
            $table->string('status');
            $table->decimal('discount', 3, 2);
            $table->text('products');
            $table->text('username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmation_o_cs');
    }
}
