<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addfieldscompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras', function($table){
            $table->string('lote')->nullable();
            $table->string('codfabrica')->nullable();
            $table->string('fechavencimiento')->default('SINFV');
            $table->string('condalmacen')->default('T.A.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function($table){
            $table->dropColumn('lote');
            $table->dropColumn('codfabrica');
            $table->dropColumn('fechavencimiento');
            $table->dropColumn('condalmacen');
        });
    }
}
