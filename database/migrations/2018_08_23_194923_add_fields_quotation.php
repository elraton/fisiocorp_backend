<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsQuotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function($table){
            $table->date('expiretime')->default('2017-01-01');
            $table->enum('status', ['annulled', 'expired', 'cancelled', 'pending', 'finished']);
            $table->integer('weeksship')->nullable();
            $table->string('username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function($table){
            $table->dropColumn('expiretime');
            $table->dropColumn('status');
            $table->dropColumn('weeksship');
            $table->dropColumn('username');
        });
    }
}
