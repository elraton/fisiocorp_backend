<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('quantity');
            $table->string('status');
            $table->string('transportedoc')->nullable();
            $table->decimal('transporte', 10, 2)->nullable()->default(0);
            $table->string('segurodoc')->nullable();
            $table->decimal('seguro', 10, 2)->nullable()->default(0);
            $table->string('advalorendoc')->nullable();
            $table->decimal('advaloren', 10, 2)->nullable()->default(0);
            $table->string('handlingdoc')->nullable();
            $table->decimal('handling', 10, 2)->nullable()->default(0);
            $table->string('inlanddoc')->nullable();
            $table->decimal('inland', 10, 2)->nullable()->default(0);
            $table->string('nacionalizaciondoc')->nullable();
            $table->decimal('nacionalizacion', 10, 2)->nullable()->default(0);
            $table->string('almacenajedoc')->nullable();
            $table->decimal('almacenaje', 10, 2)->nullable()->default(0);
            $table->string('igvdoc')->nullable();
            $table->decimal('igv', 10, 2)->nullable()->default(0);
            $table->string('username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
