<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addcostocompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras', function($table){
            $table->decimal('costocompra', 8, 2);
            $table->string('costocompradoc', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function($table){
            $table->dropColumn('costocompra');
            $table->dropColumn('costocompradoc');
        });
    }
}
