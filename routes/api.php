<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
//Route::middleware('api')->group(function () {

    Route::get('verify', function () {
        // Uses first & second Middleware
    });

    Route::get('user', 'UserController@index');
    Route::get('user/{id}', 'UserController@show');
    Route::post('user/find', 'UserController@find');
    Route::post('user', 'UserController@store');
    Route::post('user/{id}', 'UserController@update');
    Route::delete('user/{id}', 'UserController@destroy');

    Route::get('roles', 'RolesController@index');
    Route::get('roles/{id}', 'RolesController@show');
    Route::post('roles', 'RolesController@store');
    Route::post('roles/{id}', 'RolesController@update');
    Route::delete('roles/{id}', 'RolesController@destroy');

    Route::get('warehouse', 'WarehouseController@index');
    Route::post('warehouse', 'WarehouseController@store');
    Route::post('warehouse/{id}', 'WarehouseController@update');
    Route::delete('warehouse/{id}', 'WarehouseController@destroy');

    Route::post('search', 'WarehouseController@search');

    Route::get('operations', 'WarehouseOperationsController@index');
    Route::post('operations', 'WarehouseOperationsController@store');
    Route::post('operations/get_by_user', 'WarehouseOperationsController@get_by_user');
    Route::post('operations/get_by_warehouse', 'WarehouseOperationsController@get_by_warehouse');

    Route::get('customer', 'CustomersController@index');
    Route::post('customer', 'CustomersController@store');
    Route::post('customer/{id}', 'CustomersController@update');
    Route::delete('customer/{id}', 'CustomersController@destroy');

    Route::post('quotprod', 'QuotationsProductsController@store');
    Route::post('quotprod/{id}', 'QuotationsProductsController@update');
    Route::delete('quotprod/{id}', 'QuotationsProductsController@destroy');

    Route::get('compras', 'ComprasController@index');
    Route::post('compras', 'ComprasController@store');
    Route::post('compras/{id}', 'ComprasController@update');
    Route::delete('compras/{id}', 'ComprasController@destroy');

    Route::get('quotation', 'QuotationsController@index');
    Route::post('quotation', 'QuotationsController@store');
    Route::post('quotationsearch', 'QuotationsController@search');
    Route::get('quotationone/{id}', 'QuotationsController@getone');
    Route::post('quotation/{id}', 'QuotationsController@update');
    Route::post('quotationexpired/{id}', 'QuotationsController@expired');
    Route::post('confirmquotation/{id}', 'QuotationsController@confirm');
    Route::delete('quotation/{id}', 'QuotationsController@destroy');
    Route::get('quotation/last', 'QuotationsController@getlatest');
    Route::post('quotationdesc', 'QuotationsController@storeDescription');
    Route::post('quotationdesc/{id}', 'QuotationsController@updateDescription');

    Route::post('confprod', 'ConfirmationProductsController@store');
    Route::post('confprod/{id}', 'ConfirmationProductsController@update');
    Route::delete('confprod/{id}', 'ConfirmationProductsController@destroy');

    Route::get('confirmation', 'ConfirmationPurchaseOrderController@index');
    Route::post('confirmation', 'ConfirmationPurchaseOrderController@store');
    Route::post('confirmation/{id}', 'ConfirmationPurchaseOrderController@update');
    Route::delete('confirmation/{id}', 'ConfirmationPurchaseOrderController@destroy');


    Route::get('orderconfirmation', 'ConfirmationOCController@index');
    Route::post('orderconfirmation', 'ConfirmationOCController@store');
    Route::get('lastorderconfirmation', 'ConfirmationOCController@getlatest');
    Route::get('orderconfirmation/{id}', 'ConfirmationOCController@getone');
    Route::post('orderconfirmation/{id}', 'ConfirmationOCController@update');

    Route::get('guiasalida', 'GuiasalidaController@index');
    Route::get('lastguiasalida', 'GuiasalidaController@getlatest');
    Route::post('guiasalida', 'GuiasalidaController@store');
    Route::get('guiasalida/{id}', 'GuiasalidaController@getone');
    Route::post('guiasalida/{id}', 'GuiasalidaController@update');
});



//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
