<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuotationsProducts;
use App\Http\Controllers\Controller;

class QuotationsProductsController extends Controller
{
    public function store(Request $request)
    {
    	$prod = new QuotationsProducts;
        $prod->product = $request->product;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;

    	return response()->json(json_encode(["save" => $prod->save()]));
    }

    public function update(Request $request, $id) {
        $prod = QuotationsProducts::findOrFail($id);
        $prod->product = $request->product;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;

        return response()->json(json_encode(["save" => $prod->save()]));
    }

    public function destroy($id)
    {
        $prod = QuotationsProducts::findOrFail($id);
        return response()->json(json_encode(["save" => $prod->delete()]));        
    }
}
