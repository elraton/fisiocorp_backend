<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $user = User::with('roles')->get();
    	return response()->json(json_encode($user));
    }

    public function show($id)
    {
    	$user = User::findOrFail($id);
    	return response()->json(json_encode($user));
    }

    public function find(Request $request)
    {
    	$user = User::where('email', '=', $request->email)->with('roles')->first();
    	return response()->json(json_encode($user));
    }

    public function store(Request $request)
    {
    	$user = new User;
    	$user->name = $request->name;
        $user->email = $request->email;
        $user->namessurnames = $request->namessurnames;
        $user->password = bcrypt($request->password);
        $user->photo = $request->photo;
    	$user->save();

    	$rol = Role::findOrFail($request->roles);

    	$user->roles()->attach($rol);

    	return response()->json(json_encode(["save" => $user->save()]));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->namessurnames = $request->namessurnames;
        $user->email = $request->email;
        if ($request->password != '') {
            $user->password = bcrypt($request->password);
        }
        $user->photo = $request->photo;
    	$user->save();

        $rol = Role::findOrFail($request->roles);
        $user->roles()->detach();
        $user->roles()->attach($rol);

        return response()->json(json_encode(["save" => $user->save()]));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->roles()->detach();
        return response()->json(json_encode(["save" => $user->delete()]));        
    }
}
