<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConfirmationOC;
use App\Http\Controllers\Controller;

class ConfirmationOCController extends Controller
{
    public function index()
    {
        $compras = ConfirmationOC::all();

        return response()->json(json_encode($compras));
    }

    public function getlatest()
    {
        $last = ConfirmationOC::all()->last();
        return response()->json(json_encode($last));
    }

    public function getone($id)
    {
        $order = ConfirmationOC::findOrFail($id);

        return response()->json(json_encode($order));
    }

    public function store(Request $request)
    {
        $compras = new ConfirmationOC;
        $compras->client = $request->client;
        $compras->total = $request->total;
        $compras->code = $request->code;
        $compras->ship = $request->ship;
        $compras->bill = $request->bill;
        $compras->discount = $request->discount;
        $compras->products = $request->products;
        $compras->username = $request->username;
        $compras->status = $request->status;
    	return response()->json(json_encode(["save" => $compras->save()]));
    }

    public function update(Request $request, $id) {

        $compras = ConfirmationOC::findOrFail($id);
        $compras->client = $request->client;
        $compras->total = $request->total;
        $compras->code = $request->code;
        $compras->ship = $request->ship;
        $compras->bill = $request->bill;
        $compras->discount = $request->discount;
        $compras->products = $request->products;
        $compras->username = $request->username;
        $compras->status = $request->status;

    	return response()->json(json_encode(["save" => $compras->save()]));
    }
}
