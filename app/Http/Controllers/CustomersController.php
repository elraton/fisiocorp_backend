<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    public function index()
    {
        $customers = Customers::all();

        return response()->json(json_encode($customers));
    }

    public function store(Request $request)
    {
        /*$validatedData = $request->validate([
            'names' => 'required',
            'surnames' => 'required',
            'ruc' => 'nullable',
            'dni' => 'nullable|unique:dni',
            'email' => 'required|unique:email',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(json_encode(["error" => $validator]), 500);
        }*/

    	$customer = new Customers;
        $customer->names = $request->names;
        $customer->surnames = $request->surnames;
        $customer->address = $request->address;
        $customer->ruc = $request->ruc;
        $customer->dni = $request->dni;
        $customer->email = $request->email;
        $customer->phone = $request->phone;

    	return response()->json(json_encode(["save" => $customer->save()]));
    }

    public function update(Request $request, $id) {
        /*$validatedData = $request->validate([
            'names' => 'required',
            'surnames' => 'required',
            'ruc' => 'nullable',
            'dni' => 'nullable',
            'email' => 'required|unique',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(json_encode(["error" => $validator]), 500);
        }*/
        
        $customer = Customers::findOrFail($id);
        $customer->names = $request->names;
        $customer->surnames = $request->surnames;
        $customer->ruc = $request->ruc;
        $customer->address = $request->address;
        $customer->dni = $request->dni;
        $customer->email = $request->email;
        $customer->phone = $request->phone;

    	return response()->json(json_encode(["save" => $customer->save()]));
    }

    public function destroy($id)
    {
        $customer = Customers::findOrFail($id);
        return response()->json(json_encode(["save" => $customer->delete()]));        
    }
}
