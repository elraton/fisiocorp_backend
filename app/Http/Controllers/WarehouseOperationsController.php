<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse_operations;
use App\Http\Controllers\Controller;

class WarehouseOperationsController extends Controller
{
    public function index()
    {
        $operations = Warehouse_operations::all();

        return response()->json(json_encode($operations));
    }

    public function store(Request $request)
    {
        $operation = new Warehouse_operations;
        $operation->id_warehouse = $request->id_warehouse;
        $operation->id_user = $request->id_user;
        $operation->operation = $request->operation;
        $operation->quantity = $request->quantity;

    	return response()->json(json_encode(["save" => $operation->save()]));
    }

    public function get_by_user(Request $request)
    {
        $operations = Warehouse_operations::where('id_user', '=', $request->id_user)->get();
        return response()->json(json_encode($operations));
    }

    public function get_by_warehouse(Request $request)
    {
        $operations = Warehouse_operations::where('id_warehouse', '=', $request->id_warehouse)->get();
        return response()->json(json_encode($operations));
    }
}
