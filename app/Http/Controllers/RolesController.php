<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    //
    public function index()
    {
        $rol = Role::all();

        return response()->json(json_encode($rol));
    }

    public function store(Request $request)
    {
    	$rol = new Role;
    	$rol->name = $request->name;
    	$rol->description = $request->description;



    	return response()->json(json_encode(["save" => $rol->save()]));
    }

    public function update(Request $request, $id) {
        $rol = Role::findOrFail($id);
        $rol->name = $request->name;
        $rol->description = $request->description;

        return response()->json(json_encode(["save" => $rol->save()]));
    }

    public function destroy($id)
    {
        //
        $rol = Role::findOrFail($id);
        return response()->json(json_encode(["save" => $rol->delete()]));        
    }
}
