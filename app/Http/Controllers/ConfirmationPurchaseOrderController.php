<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConfirmationPurchaseOrder;
use App\ConfirmationProducts;
use App\Http\Controllers\Controller;

class ConfirmationPurchaseOrderController extends Controller
{
    public function index()
    {
        $quotations = ConfirmationPurchaseOrder::all();

        return response()->json(json_encode($quotations));
    }

    public function store(Request $request)
    {
        $quotations = new ConfirmationPurchaseOrder;
    	$quotations->client = $request->client;
        $quotations->total = $request->total;
        $arr = json_decode($request->products);
        foreach($arr as $item) {
            $prod = new ConfirmationProducts;
            $prod->product = $item['product'];
            $prod->quantity = $item['quantity'];
            $prod->price = $item['price'];
            $prod->save();
            $quotations->products()->save($prod);
        }

    	return response()->json(json_encode(["save" => $quotations->save()]));
    }

    public function update(Request $request, $id) {
        $quotations = ConfirmationPurchaseOrder::findOrFail($id);
        $quotations->client = $request->client;
        $quotations->total = $request->total;
        $arr = json_decode($request->products);
        foreach($arr as $item) {
            if ($item['id'] == 0) {
                $prod = new ConfirmationProducts;
                $prod->product = $item['product'];
                $prod->quantity = $item['quantity'];
                $prod->price = $item['price'];
                $prod->save();
                $quotations->products()->save($prod);
            } else {
                try {
                    $prod = ConfirmationProducts::findOrFail($item['id']);
                    $prod->product = $item['product'];
                    $prod->quantity = $item['quantity'];
                    $prod->price = $item['price'];
                    $prod->save();
                } catch (Exception $e) {
                    $prod = new ConfirmationProducts;
                    $prod->product = $item['product'];
                    $prod->quantity = $item['quantity'];
                    $prod->price = $item['price'];
                    $prod->save();
                    $quotations->products()->save($prod);
                }
            }
        }

        return response()->json(json_encode(["save" => $rol->save()]));
    }

    public function destroy($id)
    {
        $rol = ConfirmationPurchaseOrder::findOrFail($id);
        return response()->json(json_encode(["save" => $rol->delete()]));        
    }
}
