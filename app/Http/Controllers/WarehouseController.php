<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;
use App\Http\Controllers\Controller;

class WarehouseController extends Controller
{
    public function index()
    {
        $warehouse = Warehouse::all();

        return response()->json(json_encode($warehouse));
    }

    public function search(Request $request)
    {
        $warehouse = Warehouse::where('name','like','%'.$request->search.'%')
        ->orWhere('code','like','%'.$request->search.'%')
        ->orWhere('description','like','%'.$request->search.'%')->get();

    	return response()->json(json_encode($warehouse));
    }

    public function store(Request $request)
    {
        $warehouse = new Warehouse;
        $warehouse->code = $request->code;
        $warehouse->category = $request->category;
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        $warehouse->accesories = $request->accesories;
        $warehouse->quantity = $request->quantity;
        $warehouse->quantity_now = $request->quantity_now;
        $warehouse->status = $request->status;
        $warehouse->serie_number = $request->serie_number;
        $warehouse->spares = $request->spares;
        $warehouse->price = $request->price;
        $warehouse->utilidad = $request->utilidad;

        $warehouse->lote = $request->lote;
        $warehouse->codfabrica = $request->codfabrica;
        $warehouse->fechavencimiento = $request->fechavencimiento;
        $warehouse->condalmacen = $request->condalmacen;

        $warehouse->qtyreserved = $request->qtyreserved;

        $warehouse->save();

    	return response()->json(json_encode($warehouse));
    }

    public function update(Request $request, $id) {
        $warehouse = Warehouse::findOrFail($id);
        $warehouse->code = $request->code;
        $warehouse->category = $request->category;
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        $warehouse->accesories = $request->accesories;
        $warehouse->quantity = $request->quantity;
        $warehouse->quantity_now = $request->quantity_now;
        $warehouse->status = $request->status;
        $warehouse->serie_number = $request->serie_number;
        $warehouse->spares = $request->spares;
        $warehouse->price = $request->price;
        $warehouse->utilidad = $request->utilidad;

        $warehouse->lote = $request->lote;
        $warehouse->codfabrica = $request->codfabrica;
        $warehouse->fechavencimiento = $request->fechavencimiento;
        $warehouse->condalmacen = $request->condalmacen;
        $warehouse->qtyreserved = $request->qtyreserved;

        return response()->json(json_encode(["save" => $warehouse->save()]));
    }

    public function destroy($id)
    {
        //
        $warehouse = Warehouse::findOrFail($id);
        return response()->json(json_encode(["save" => $warehouse->delete()]));        
    }
}
