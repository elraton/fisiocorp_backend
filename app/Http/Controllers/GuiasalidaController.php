<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guiasalida;
use App\Http\Controllers\Controller;

class GuiasalidaController extends Controller
{
    public function index()
    {
        $guias = Guiasalida::all();

        return response()->json(json_encode($guias));
    }

    public function getlatest()
    {
        try {
            $guia = Guiasalida::all()->last();
        } catch (Exception $e) {
            $guia = [];
        }
        return response()->json(json_encode($guia));
    }

    public function store(Request $request)
    {
        $guia = new Guiasalida;
        $guia->code = $request->code;
        $guia->codeoc = $request->codeoc;
        $guia->outdate = $request->outdate;
        $guia->client = $request->client;
        $guia->status = $request->status;
        $guia->products = $request->products;

    	return response()->json(json_encode(["save" => $guia->save()]));
    }

    public function update(Request $request, $id) {
        
        $guia = Guiasalida::findOrFail($id);
        $guia->code = $request->code;
        $guia->codeoc = $request->codeoc;
        $guia->outdate = $request->outdate;
        $guia->client = $request->client;
        $guia->status = $request->status;
        $guia->products = $request->products;

    	return response()->json(json_encode(["save" => $guia->save()]));
    }

    public function getone($id)
    {
        $guia = Guiasalida::findOrFail($id);
        return response()->json(json_encode($guia));        
    }
}
