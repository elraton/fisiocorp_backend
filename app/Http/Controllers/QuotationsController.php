<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotations;
use App\QuotationsProducts;
use App\Productdescription;
use App\ConfirmationOC;
use App\Http\Controllers\Controller;

class QuotationsController extends Controller
{
    public function index()
    {
        $quotations = Quotations::with('products')->get();

        return response()->json(json_encode($quotations));
    }

    public function getlatest()
    {
        $user = Quotations::all()->last();
        return response()->json(json_encode($user));
    }

    public function search(Request $request)
    {
        $search = Quotations::where('code','like','%'.$request->search.'%')
        ->orWhere('created_at','like','%'.$request->search.'%');

    	return response()->json(json_encode($search));
    }

    public function getone($id)
    {
        $quot = Quotations::with('products')->with('descriptions')->findOrFail($id);

        $desc = json_decode($quot->descriptions);
        foreach ($desc as $dess) {
            $img_data = file_get_contents($dess->image);
            $base64 = base64_encode($img_data);
            $imgaux = 'data: '.mime_content_type($dess->image).';base64,'.$base64;
            $dess->image = $imgaux;
        }
        return response()->json(json_encode( ["quotation" => $quot, "base64" => $desc] ));

        return response()->json(json_encode($quot));
    }

    public function storeDescription(Request $request)
    {
        $quotation = Quotations::findOrFail($request->idquot);
        $prod = new Productdescription;
        $prod->description = $request->description;
        $prod->title = $request->title;

        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $filename = time().'.'.$extension;
        $image->move('uploads/products/', $filename);
        $prod->image = 'uploads/products/' . $filename;
        $prod->save();
        $quotation->descriptions()->save($prod);
        return response()->json(json_encode(["save" => "ok"]));
    }

    public function updateDescription(Request $request, $id)
    {
        // $quotation = Quotations::findOrFail($id);
        $prod = Productdescription::findOrFail($id);
        $prod->description = $request->description;

        $image = $request->file('image');
        if ($image) {
            $extension = $image->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $image->move('uploads/products/', $filename);
            $prod->image = 'uploads/products/' . $filename;
        }
        $prod->save();
        // $quotation->descriptions()->save($prod);
        return response()->json(json_encode(["save" => "ok"]));
    }

    public function store(Request $request)
    {
        $quotations = new Quotations;
    	$quotations->client = $request->client;
        $quotations->total = $request->total;
        $quotations->expiretime = $request->expiretime;
        $quotations->status = $request->status;
        $quotations->weeksship = $request->weeksship;
        $quotations->username = $request->username;
        $quotations->city = $request->city;
        $quotations->name = $request->name;
        $quotations->charge = $request->charge;
        $quotations->code = $request->code;
        $quotations->payment = $request->payment;
        $quotations->ship_place = $request->ship_place;
        $quotations->isequipment = $request->isequipment;
        
        $quotations->save();
        $arr = json_decode($request->products, true);
        foreach($arr as $item) {

            $prod = new QuotationsProducts;
            $prod->product = $item['product'];
            $prod->quantity = $item['quantity'];
            $prod->price = $item['price'];
            $prod->save();
            $quotations->products()->save($prod);
        }

    	return response()->json(json_encode(["save" => json_encode($quotations)]));
    }
    

    public function confirm(Request $request, $id) {
        $quotation = Quotations::findOrFail($id);
        $quotation->status = 'finished';
        $quotation->save();

        $conf = new ConfirmationOC;
        $conf->client = $request->client;
        $conf->total = $request->total;
        $conf->code = $request->code;
        $conf->ship = $request->ship;
        $conf->bill = $request->bill;
        $conf->status = $request->status;
        $conf->discount = $request->discount;
        $conf->products = $request->products;
        $conf->username = $request->username;
        $conf->quotation = $request->quotation;
        $conf->save();

        return response()->json(json_encode(["save" => "ok"]));
    }

    public function expired(Request $request, $id) {
        $quotation_old = Quotations::findOrFail($id);
        $quotation_old->status = 'expired';
        $quotation_old->save();

        return response()->json(json_encode(["save" => json_encode($quotation_old)]));
    }


    public function update(Request $request, $id) {
        $quotation_old = Quotations::findOrFail($id);
        $quotation_old->status = 'annulled';
        $quotation_old->save();

        $quotation_new = new Quotations;
    	$quotation_new->client = $request->client;
        $quotation_new->total = $request->total;
        $quotation_new->expiretime = $request->expiretime;
        $quotation_new->status = $request->status;
        $quotation_new->weeksship = $request->weeksship;
        $quotation_new->username = $request->username;
        $quotation_new->city = $request->city;
        $quotation_new->name = $request->name;
        $quotation_new->charge = $request->charge;
        $quotation_new->code = $request->code;
        $quotation_new->payment = $request->payment;
        $quotation_new->ship_place = $request->ship_place;
        $quotation_new->isequipment = $request->isequipment;
        $quotation_new->save();

        $arr = json_decode($request->products, true);
        foreach($arr as $item) {

            $prod = new QuotationsProducts;
            $prod->product = $item['product'];
            $prod->quantity = $item['quantity'];
            $prod->price = $item['price'];
            $prod->save();
            $quotation_new->products()->save($prod);
        }

        return response()->json(json_encode(["save" => json_encode($quotation_new)]));
    }

    public function destroy($id)
    {
        $rol = Quotations::findOrFail($id);
        return response()->json(json_encode(["save" => $rol->delete()]));        
    }
}
