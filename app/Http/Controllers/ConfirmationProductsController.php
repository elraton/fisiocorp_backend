<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConfirmationProducts;
use App\Http\Controllers\Controller;

class ConfirmationProductsController extends Controller
{
    public function store(Request $request)
    {
    	$prod = new ConfirmationProducts;
        $prod->product = $request->product;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;

    	return response()->json(json_encode(["save" => $prod->save()]));
    }

    public function update(Request $request, $id) {
        $prod = ConfirmationProducts::findOrFail($id);
        $prod->product = $request->product;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;

        return response()->json(json_encode(["save" => $prod->save()]));
    }

    public function destroy($id)
    {
        $prod = ConfirmationProducts::findOrFail($id);
        return response()->json(json_encode(["save" => $prod->delete()]));        
    }
}
