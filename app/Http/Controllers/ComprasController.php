<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\compras;
use App\Http\Controllers\Controller;

class ComprasController extends Controller
{
    public function index()
    {
        $compras = compras::all();

        return response()->json(json_encode($compras));
    }

    public function store(Request $request)
    {
        $compras = new compras;
        $compras->name = $request->name;
        $compras->description = $request->description;

        $compras->category = $request->category;
        $compras->code = $request->code;
        $compras->accesories = $request->accesories;
        $compras->serienumber = $request->serienumber;
        $compras->repuestos = $request->repuestos;

        $compras->costocompradoc = $request->costocompradoc;
        $compras->costocompra = $request->costocompra;


        $compras->quantity = $request->quantity;
        $compras->status = $request->status;
        $compras->transportedoc = $request->transportedoc;
        $compras->transporte = $request->transporte;
        $compras->segurodoc = $request->segurodoc;
        $compras->seguro = $request->seguro;
        $compras->advalorendoc = $request->advalorendoc;
        $compras->advaloren = $request->advaloren;
        $compras->handlingdoc = $request->handlingdoc;
        $compras->handling = $request->handling;
        $compras->inlanddoc = $request->inlanddoc;
        $compras->inland = $request->inland;
        $compras->nacionalizaciondoc = $request->nacionalizaciondoc;
        $compras->nacionalizacion = $request->nacionalizacion;
        $compras->almacenajedoc = $request->almacenajedoc;
        $compras->almacenaje = $request->almacenaje;
        $compras->igvdoc = $request->igvdoc;
        $compras->igv = $request->igv;
        $compras->username = $request->username;

        $compras->lote = $request->lote;
        $compras->codfabrica = $request->codfabrica;
        $compras->fechavencimiento = $request->fechavencimiento;
        $compras->condalmacen = $request->condalmacen;

    	return response()->json(json_encode(["save" => $compras->save()]));
    }

    public function update(Request $request, $id) {

        $compras = compras::findOrFail($id);
        if ($compras->status != 'confirmada') {
            $compras->name = $request->name;
            $compras->description = $request->description;

            $compras->category = $request->category;
            $compras->code = $request->code;
            $compras->accesories = $request->accesories;
            $compras->serienumber = $request->serienumber;
            $compras->repuestos = $request->repuestos;

            $compras->quantity = $request->quantity;
            $compras->status = $request->status;

            $compras->costocompradoc = $request->costocompradoc;
            $compras->costocompra = $request->costocompra;

            $compras->transportedoc = $request->transportedoc;
            $compras->transporte = $request->transporte;
            $compras->segurodoc = $request->segurodoc;
            $compras->seguro = $request->seguro;
            $compras->advalorendoc = $request->advalorendoc;
            $compras->advaloren = $request->advaloren;
            $compras->handlingdoc = $request->handlingdoc;
            $compras->handling = $request->handling;
            $compras->inlanddoc = $request->inlanddoc;
            $compras->inland = $request->inland;
            $compras->nacionalizaciondoc = $request->nacionalizaciondoc;
            $compras->nacionalizacion = $request->nacionalizacion;
            $compras->almacenajedoc = $request->almacenajedoc;
            $compras->almacenaje = $request->almacenaje;
            $compras->igvdoc = $request->igvdoc;
            $compras->igv = $request->igv;
            $compras->username = $request->username;

            $compras->lote = $request->lote;
            $compras->codfabrica = $request->codfabrica;
            $compras->fechavencimiento = $request->fechavencimiento;
            $compras->condalmacen = $request->condalmacen;
        }

    	return response()->json(json_encode(["save" => $compras->save()]));
    }

    public function destroy($id)
    {
        $compras = compras::findOrFail($id);
        return response()->json(json_encode(["save" => $compras->delete()]));        
    }
}
