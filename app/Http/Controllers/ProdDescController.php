<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productdescription;
use App\Http\Controllers\Controller;

class ProdDescController extends Controller
{
    public function getDescriptions($id)
    {
        $quotations = Productdescription::with('products')->get();

        return response()->json(json_encode($quotations));
    }

    public function store(Request $request)
    {
        $quotations = new Productdescription;
    	$quotations->client = $request->client;
        $quotations->total = $request->total;
        $quotations->expiretime = $request->expiretime;
        $quotations->status = $request->status;
        $quotations->weeksship = $request->weeksship;
        $quotations->username = $request->username;

        $quotations->name = $request->name;
        $quotations->charge = $request->charge;
        $quotations->code = $request->code;
        $quotations->save();
        $arr = json_decode($request->products, true);
        foreach($arr as $item) {

            $prod = new QuotationsProducts;
            $prod->product = $item['product'];
            $prod->quantity = $item['quantity'];
            $prod->price = $item['price'];
            $prod->save();
            $quotations->products()->save($prod);
        }

    	return response()->json(json_encode(["save" => json_encode($quotations)]));
    }

    public function update(Request $request, $id) {
        $quotation_old = Quotations::findOrFail($id);
        $quotation_old->status = 'annulled';
        $quotation_old->save();

        $quotation_new = new Quotations;
    	$quotation_new->client = $request->client;
        $quotation_new->total = $request->total;
        $quotation_new->expiretime = $request->expiretime;
        $quotation_new->status = $request->status;
        $quotation_new->weeksship = $request->weeksship;
        $quotation_new->username = $request->username;

        $quotations->name = $request->name;
        $quotations->charge = $request->charge;
        $quotations->code = $request->code;
        $arr = json_decode($request->products, true);
        foreach($arr as $item) {

            $prod = new QuotationsProducts;
            $prod->product = $item['product'];
            $prod->quantity = $item['quantity'];
            $prod->price = $item['price'];
            $prod->save();
            $quotation_new->products()->save($prod);
        }

        return response()->json(json_encode(["save" => $quotation_new->save()]));
    }
}
