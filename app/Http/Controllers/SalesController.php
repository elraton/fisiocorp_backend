<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sales;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    public function index()
    {
        $sale = Sales::all();

        return response()->json(json_encode($sale));
    }

    public function store(Request $request)
    {
        $sale = new Sales;
        $sale->code = $request->code;
        $sale->customer = $request->customer;
        $sale->quantity = $request->quantity;
        $sale->product = $request->product;

    	return response()->json(json_encode(["save" => $sale->save()]));
    }

    public function update(Request $request, $id) {
        $sale = Sales::findOrFail($id);
        $sale->code = $request->code;
        $sale->customer = $request->customer;
        $sale->quantity = $request->quantity;
        $sale->product = $request->product;

        return response()->json(json_encode(["save" => $sale->save()]));
    }

    public function destroy($id)
    {
        //
        $sale = Sales::findOrFail($id);
        return response()->json(json_encode(["save" => $sale->delete()]));        
    }
}
