<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productdescription extends Model
{
    public function quotation()
    {
        return $this->belongsToMany('App\Quotations');
    }
}
