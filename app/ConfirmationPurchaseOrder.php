<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmationPurchaseOrder extends Model
{
    public function products() {
        return $this->hasMany('App\ConfirmationProducts');
    }
}
