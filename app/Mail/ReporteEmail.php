<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReporteEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    public function __construct($data2 = "")
    {
        //
        $this->data = $data2;
    }

    public function build()
    {

        $file1 = $this->data['file1'];
        $file2 = $this->data['file2'];
        $file3 = $this->data['file3'];
        $address = 'no-reply@fisiovida.com';
        $subject = 'Reporte Diario de Asistencias';
        $name = 'Asistencia Fisiovida';

        return $this->view('emails.test')
                    ->from($address, $name)
                    ->subject($subject)
                    ->attach($file1)
                    ->attach($file2)
                    ->attach($file3);
                    //->with([ 'message' => $daata ]);
    }
}
