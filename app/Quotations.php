<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotations extends Model
{
    //
    public function products() {
        return $this->belongsToMany('App\QuotationsProducts');
    }
    public function descriptions() {
        return $this->belongsToMany('App\Productdescription');
    }
}
